from __future__ import annotations
import pygame
from .screen import Screen
from .game_screen import GameScreen
from .. import window as win


class MainMenuScreen(Screen):
    def __init__(self, window: win.Window):
        super().__init__(window)
        font = window.asset_factory.get_font("main_font.ttf")
        self.background_image = window.asset_factory.get_image("main_window_background.jpg")
        self.text = font.render("Press F to pay respect", False, (255, 255, 255))
        self.text_size = self.text.get_size()

    def handle_scene_event(self, event: pygame.event.Event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_f:
                self.__change_screen()

    def render_scene(self, surface: pygame.Surface):
        surface.blit(self.background_image, (0, 0))
        text_x = self.window.width / 2 - self.text_size[0] / 2
        text_y = self.window.height - self.text_size[1] - 15
        surface.blit(self.text, (text_x, text_y))

    def __change_screen(self):
        self.window.set_screen(GameScreen(self.window))
