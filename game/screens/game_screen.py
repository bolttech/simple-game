from __future__ import annotations
import pygame
from .screen import Screen
from .. import window as win
from ..objects.main_character import MainCharacter


class GameScreen(Screen):
    def __init__(self, window: win.Window):
        super().__init__(window)
        self.player = MainCharacter()
        self.player.x = window.width / 2 - 16
        self.player.y = window.height / 2 - 16
        self.spawn(self.player)

    def render_scene(self, surface: pygame.Surface):
        surface.fill((0, 255, 255))
