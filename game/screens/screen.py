from __future__ import annotations
import pygame
from .. import window as win
from ..objects import game_object as go


class Screen:
    objects: list[go.GameObject]

    def __init__(self, window: win.Window):
        self.window = window
        self.objects = []

    def spawn(self, obj: go.GameObject):
        self.objects.append(obj)

    def kill(self, obj: go.GameObject):
        self.objects.remove(obj)

    def handle_event(self, event: pygame.event.Event):
        self.handle_scene_event(event)
        for obj in self.objects:
            obj.handle_event(event)

    def process(self, time_delta: float):
        self.process_scene(time_delta)
        for obj in self.objects:
            obj.process(time_delta)

    def render(self, surface: pygame.Surface):
        self.render_scene(surface)
        for obj in self.objects:
            obj.render(surface)

    def handle_scene_event(self, event: pygame.event.Event):
        pass

    def process_scene(self, time_delta: float):
        pass

    def render_scene(self, surface: pygame.Surface):
        pass
