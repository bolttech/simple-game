import os


root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))   # __file__/../..
assets_dir = os.path.join(root_dir, "assets")
textures_dir = os.path.join(assets_dir, "textures")
fonts_dir = os.path.join(assets_dir, "fonts")
