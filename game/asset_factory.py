import os.path
import pygame
from . import fs


class AssetFactory:
    def __init__(self):
        self.textures = {}
        self.fonts = {}

    def get_image(self, name: str) -> pygame.Surface:
        if name not in self.textures:
            image_path = os.path.join(fs.textures_dir, name)
            image = pygame.image.load(image_path).convert()
            self.textures[name] = image

        return self.textures[name]

    def get_font(self, name: str) -> pygame.font.Font:
        if name not in self.fonts:
            font_path = os.path.join(fs.fonts_dir, name)
            font = pygame.font.Font(font_path, 24)
            self.fonts[name] = font

        return self.fonts[name]
