from __future__ import annotations
from datetime import datetime
import pygame
from game.screens import screen as scr
#from game.screens import main_menu_screen as mws
from game.screens import game_screen as gs
from .asset_factory import AssetFactory


class Window:
    INITIALIZED = False

    def __init__(self, width: int, height: int, title: str):
        self.width = width
        self.height = height
        self.title = title

        if not Window.INITIALIZED:
            pygame.init()
            Window.INITIALIZED = True

        self.__asset_factory = AssetFactory()

        self.window = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption(self.title)

        self.running = False

        self.last_frame_time = datetime.now()

        self.screen = gs.GameScreen(self)

    @property
    def asset_factory(self) -> AssetFactory:
        return self.__asset_factory

    def set_screen(self, screen: scr.Screen):
        self.screen = screen

    def run(self):
        self.running = True
        while self.running:
            self.__handle_events()
            self.__process()
            self.__render()

    def __handle_events(self):
        """
        Реакция на события извне
        """
        for event in pygame.event.get():  # event loop
            if event.type == pygame.QUIT:
                self.running = False
            else:
                self.screen.handle_event(event)

    def __process(self):
        """
        Действия, которые происходят вне зависимости от внешних событий
        """
        current_time = datetime.now()
        time_delta = current_time - self.last_frame_time
        self.last_frame_time = current_time
        time_delta = time_delta.total_seconds()

        self.screen.process(time_delta)

    def __render(self):
        """
        Отрисовка
        """
        self.window.fill((0, 0, 255))

        self.screen.render(self.window)

        pygame.display.flip()
