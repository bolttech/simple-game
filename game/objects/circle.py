from __future__ import annotations
import pygame
from game.objects.game_object import GameObject


class Circle(GameObject):
    def __init__(self, center_x: int, center_y: int, radius: int):
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius
        self.x_direction = 0
        self.y_direction = 0
        self.speed = 300

    def handle_event(self, event: pygame.event.Event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                self.y_direction = -1
            elif event.key == pygame.K_s:
                self.y_direction = 1
            elif event.key == pygame.K_d:
                self.x_direction = 1
            elif event.key == pygame.K_a:
                self.x_direction = -1
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_w or event.key == pygame.K_s:
                self.y_direction = 0
            elif event.key == pygame.K_d or event.key == pygame.K_a:
                self.x_direction = 0

    def process(self, time_delta: float):
        self.center_x += self.x_direction * self.speed * time_delta
        self.center_y += self.y_direction * self.speed * time_delta

    def render(self, surface: pygame.Surface):
        pygame.draw.circle(surface, (255, 0, 0), (self.center_x, self.center_y), 50)
        pygame.draw.circle(surface, (0, 255, 0), (self.center_x, self.center_y), 50, 3)
