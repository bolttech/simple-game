from __future__ import annotations
import math
import pygame
from game.objects.game_object import GameObject


class MainCharacter(GameObject):
    def __init__(self):
        self.skin = self.__draw_character()
        self.actual_skin = self.skin
        self.x = 10
        self.y = 10
        self.skin_rect = self.skin.get_rect(topleft=(self.x, self.y))

    def handle_event(self, event: pygame.event.Event):
        if event.type == pygame.MOUSEMOTION:
            self.__rotate()

    def process(self, time_delta: float):
        pass

    def render(self, surface: pygame.Surface):
        surface.blit(self.actual_skin, self.skin_rect)

    def __draw_character(self) -> pygame.Surface:
        result = pygame.Surface((32, 32), pygame.SRCALPHA).convert_alpha()
        pygame.draw.rect(result, (0, 255, 0), (0, 5, 32, 22), border_radius=5)
        pygame.draw.rect(result, (0, 0, 0), (0, 5, 32, 22), 3, border_radius=5)
        pygame.draw.line(result, (0, 0, 0), (16, 2), (16, 5), 3)
        return result

    def __rotate(self):
        x1, y1 = pygame.mouse.get_pos()
        x2 = self.x + 16
        y2 = self.y + 16
        angle = math.degrees(math.atan2(abs(x2 - x1), abs(y2 - y1)))
        self.actual_skin = pygame.transform.rotate(self.skin, angle)
        self.skin_rect = self.actual_skin.get_rect(
            center=self.skin.get_rect(center=(self.x + 16, self.y + 16)).center
        )
