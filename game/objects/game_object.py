from __future__ import annotations
from typing import Protocol
from abc import abstractmethod
import pygame


class GameObject(Protocol):
    @abstractmethod
    def handle_event(self, event: pygame.event.Event):
        pass

    @abstractmethod
    def process(self, time_delta: float):
        pass

    @abstractmethod
    def render(self, surface: pygame.Surface):
        pass
